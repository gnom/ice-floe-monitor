import network
import utime
import ujson


with open("boottime", "w") as fh:
    fh.write(str(utime.time()))

def get_wifi_config():
    try:
        with open("wificonfig.json", "r") as fh:
            wificonfig = ujson.load(fh)
        return wificonfig["ssid"], wificonfig["password"]
    except:
        print("Unable to load wifi config")
        print("Did you copy wificonfig.example.json to wificonfig.json?")
        return False

def do_connect(wificred):
    ap_if = network.WLAN(network.AP_IF)
    ap_if.active(False)
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print("connecting to network...")
        sta_if.active(True)
        sta_if.connect("Antarktis", "Pinguine1337")
        while not sta_if.isconnected():
            pass
    print("network config:", sta_if.ifconfig())


wificred = get_wifi_config()
if wificred:
    do_connect(wificred)
