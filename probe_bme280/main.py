from machine import Pin, I2C
import utime
import BME280
import socket
import ujson

i2c = I2C(scl=Pin(5), sda=Pin(4), freq=10000)

with open("boottime", "r") as fh:
    BOOTTIME = int(fh.read())

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(("", 80))
sock.listen(5)

def json_sensor():
    global i2c
    bme = BME280.BME280(i2c=i2c)
    measurement = {
        "measurements": {
            "temperature": {"value": bme.temperature, "unit": "°C",},
            "humidity": {"value": bme.humidity, "unit": "%",},
            "pressure": {"value": bme.pressure, "unit": "hPa"}
        },
        "device": {
            "voltage": {"value": None, "unit": "V"},
            "uptime": {"value": utime.time() - BOOTTIME},
        },
    }
    return ujson.dumps(measurement)

_ = json_sensor()

while True:
    conn, addr = sock.accept()
    print("Connection from {}".format(str(addr)))
    request = conn.recv(1024)
    request = str(request.decode("ascii"))
    print("Request: {}".format(request))
    try:
        sensor_data = json_sensor()
        print(sensor_data)
        conn.send("HTTP/1.1 200 OK\n")
        conn.send("Content-Type: application/json\n")
        conn.send("Connection: close\n\n")
        conn.send(sensor_data)
        conn.close()
    except BaseException as err:
        conn.send("HTTP/1.1 404 Not Found\n\n")
        conn.send("sensor not found\n")
        print("Error: ", err)
        conn.close()
